
public class ListaFunc {
	
	private NoPessoa primeiro;
	private NoPessoa ultimo;
	
	
	public void inicializarLista(NoPessoa noPessoa){
		primeiro = noPessoa;
		ultimo = primeiro;
	}
		
	public void adcionarInicio(NoPessoa noPessoa){
		if(primeiro == null){
			inicializarLista(noPessoa);
		}else{
			noPessoa.setProximo(primeiro);
			primeiro = noPessoa;
		}
	}
	
	public void imprimirLista(){
		NoPessoa aux = primeiro;
		boolean lop = true;
		while (lop) {
			System.out.println(aux.getNome());
			if(aux.getProximo()!= null){
				aux = aux.getProximo();
			}else{
				return;
			}
		}
	}
	
	
	
	
	public NoPessoa getPrimeiro() {
		return primeiro;
	}
	public void setPrimeiro(NoPessoa primeiro) {
		this.primeiro = primeiro;
	}
	public NoPessoa getUltimo() {
		return ultimo;
	}
	public void setUltimo(NoPessoa ultimo) {
		this.ultimo = ultimo;
	}
	
	

}
